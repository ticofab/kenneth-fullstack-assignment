'use strict'
let should = require('should')

let ApiUtils = require('./api-utils')

describe('ApiUtils', function () {
  describe('should not be instantiated', function () {
    let err
    try {
      let utils = new ApiUtils()
      utils
    } catch (error) {
      err = error
    }
    should.exist(err)
  })

  describe('.parseQuery', function () {
    xit('should parse arrays correctly', function () {
      let input = { arr: ['one', 'two', 'three'] }
      let output = ApiUtils.parseQuery(input)
      output.should.have.properties({
        filter: { arr: { $in: ['one', 'two', 'three'] } },
        computedFields: {}
      })
    })

    xit('should parse computed fields correctly', function () {
      let input = { $arr: ['one', 'two', 'three'] }
      let output = ApiUtils.parseQuery(input)
      output.should.have.properties({
        filter: {},
        computedFields: { arr: ['one', 'two', 'three'] }
      })
    })
  })

  describe('.parsePhoneNumber', function () {
    xit('should remove leading zeroes', function () {
      let input = '0032123456'
      let output = ApiUtils.parsePhoneNumber(input)
      output.should.eql('32123456')
    })

    xit('should remove spaces, dots, stripes and all the other stuff', function () {
      let input = '+32 12/3.4-5 6'
      let output = ApiUtils.parsePhoneNumber(input)
      output.should.eql('32123456')
    })
  })

  describe('.success', function (done) {
    xit('should not send a response when response has already been sent', function () {
      class res {
        static get headersSent () { return true }
        static status (statuscode) {
          throw new Error('res.status should not be called')
        }
        static json (payload) {
          throw new Error('res.json should not be called')
        }
      }
      ApiUtils.success(res)(200)
    })

    xit('should send a response when response has already been sent', function () {
      let statusCalled = false
      let jsonCalled = false
      class res {
        static get headersSent () { return false }
        static status (statuscode) {
          statusCalled = true
          return this
        }
        static json (payload) {
          jsonCalled = true
          return this
        }
      }
      ApiUtils.success(res)(200)

      statusCalled.should.eql(true)
      jsonCalled.should.eql(true)
    })
  })
})
