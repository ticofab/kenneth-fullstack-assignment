'use strict'
/**
 * @module utils/api-utils
 */
class ApiUtils {
  constructor () {
    throw new Error('Api utils is a static class and should not be instantiated')
  }

  /**
   * Returns a promise callback that can send a HTTP response
   * @static
   * @param  {Object} res             HTTP Response
   * @param  {Number} statuscode=200  HTTP Status code
   * @return {module:utils/api-utils.successCallback}  Callback
   */
  static success (res, statuscode) {
    return function sendResponse (payload) {
      if (res.headersSent) {
        console.warn('apiUtils.success called after response has already been sent! Check middleware!')
        console.warn(new Error().stack)
        return
      }

      if (res && !res.headersSent) {
        return res.status(statuscode || 200).json(payload)
      }
    }
  }

  /**
   * Sends a HTTP response with a json payload.
   * @callback module:utils/api-utils.successCallback
   * @param {payload} payload
  */

  static parsePhoneNumber (phoneNumber) {
    if (!phoneNumber){return phoneNumber}
    phoneNumber = phoneNumber.replace(/\W/g, '')
    if (phoneNumber.indexOf('00') === 0) {
      phoneNumber = phoneNumber.substr(2)
    }
    return phoneNumber
  }

  static parseQuery (query) {

    console.log("parseQuery -> ",query)

    let filter = {}
    let computedFields = {}

    Object.keys(query).forEach(function (key) {
      let value = query[key]

      // Parse special parameters
      if (key.charAt(0) === '$') {
        computedFields[key.slice(1)] = value
        return
      }

      // Parse arrays  to allow filtering on multiple parameters
      if (value.constructor === Array) {
        value = {$in: value}
      }

      filter[key] = value
    })

    return {filter, computedFields}
  }
}

exports = module.exports = ApiUtils
