/**
 * Main application routes
 */

'use strict'
let Config = require('config')
let passport = require('passport')
let Model = require('../lib/model')
let User = Model.User
let AuthCode = Model.AuthCode
let Forecast = Model.Forecast

module.exports = function (app) {
  app.use('/', require('express').static('public'))

  // Resource routes
  app.use('/api/users', require('./api/user'))

  // Functional routes
  app.use('/auth', require('./auth'))
  app.use('/api/me', require('./api/me'))

  require(__dirname + '/auth/authroute')(app, passport, User, AuthCode);

  require(__dirname + '/forecasts/forecastsrouter')(app, Forecast)

  // APIdoc endpoint
  app.use('/docs', require('express').static('apidoc'))
  app.use('/jsdoc', require('express').static('documentation-output'))

  // redirect url
  app.get('/redirect/*', function (req, res) {
    let path = req.path.slice(10)
    let protocol = Config.client.ios.protocol

    let uri = `${protocol}://${path}`

    console.debug('redirecting to %s', uri)
    res.append('Location', uri)
    res.status(302).end()
  })

  // All undefined asset or api routes should return a 404
  app.route('/:url(api|auth|components|app|bower_components|assets)/*')
  .get(function pageNotFound(req, res, next) {
    // FIXME move to errorHandler
    let viewFilePath = '404'
    let statusCode = 404
    let result = {
      status: statusCode
    }

    res.status(result.status)
    res.render(viewFilePath, function (err) {
      if (err) {
        return res.json(result, result.status)
      }

      res.render(viewFilePath)
    })
  })


  // All other routes should redirect to the index.html
  app.route('/*')
  .get(function (req, res) {
    res.status(404).render("404")
  })
}
