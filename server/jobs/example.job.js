'use strict'

class ExampleJob {
  constructor () {}

  // Used to expose methods to unit tests
  static get _test () {
    return {
    }
  }

  // Returns a promise
  start () {
    console.debug('[JOB] Example: Starting')
    console.debug('[JOB] Example: Done')
  }
}

module.exports = ExampleJob
