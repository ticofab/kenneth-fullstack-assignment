'use strict';

//TODO: CRUD operations with Forecast entity

module.exports = function (app, Forecast) {

  app.get('/forecasts', function (req, res) {
    return res.render('forecasts/index', { forecasts: {} });
  });


  app.get('/forecasts/edit', function (req, res) {
    res.render('forecasts/edit', { forecasts: {}, flash: {} });
  });

  app.post('/forecasts', function (req, res) {

    return res.render('forecasts/edit', { forecasts: {} });
  });

  app.get('/forecasts/:id', function (req, res) {

    return res.render('forecasts/edit', { forecasts: {}, flash: 'Created.' });
  });

  app.post('/forecasts/:id', function (req, res) {

    return res.render('forecasts/edit', { forecasts: {}, flash: 'Saved.' });
  });

  app.delete('/forecasts/:id', function (req, res) {

    return res.render('index', { flash: 'Forecasts deleted.' });
  });

  app.post('/forecasts_webhook', function (req, res) {
    return res.json([]);
  })

  return app
}
