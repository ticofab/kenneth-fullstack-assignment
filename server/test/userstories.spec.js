// Hello there developer,
// We have prepared a little test skeleton for you
// Here you will find the basic test scenarios you will have to implement
// You will be done when all them are implemented and testing as required

//TODO: Modify to use your just created model forecast
//TODO: Implement all tests and have all running green


'use strict'

let request = require('supertest')
let should = require('should')
let bb = require("bluebird")
var moment = require("moment")

let app = require('../app')

let utils = require('./utils')

let Model = require('../../lib/model')
let User = Model.User

let Forecast = Model.Forecast

let UserMocks = User._mocks
let ForecastMocks = Forecast._mocks

// let SECURITY_FIELDS = ['hashedPassword', 'salt']
let Chance = require('chance')
let chance = new Chance()
let authTest = require('./auth')

describe('User Stories: ', function () {
  let user
  let token
  let appleToken = chance.apple_token()
  let gender = chance.gender()
  let first = chance.first({ gender: gender })
  let last = chance.last({ gender: gender })
  // let email = [first, '.', last, '@', chance.domain()].join('')
  let email = "youremail@gmail.com"
  let role = 'admin'
  let isOperator = true

  let testUser = {
    _id: chance.hash({ length: 24 }),
    phoneNumber: '32485' + chance.string({ length: 6, pool: '0123456789' }),
    powers: { canWork: true },
    password: chance.hash(),
    apnDeviceToken: appleToken,
    gender: gender,
    firstName: first,
    lastName: last,
    email: email,
    schedule: {
      schedules: [{
        d: [2, 3, 4, 5, 6],
        h: [9, 10, 11, 12, 14, 15, 16, 17]
      }]
    },
    role: role,
    isOperator: isOperator
  }

  // Clear users before testing
  before(function (done) {
    utils
    .resetDB([Forecast])
    .then(function (userToken) {
      // Set auth token
      token = userToken
      return User.create(testUser)
    })
    .then(function (createdUser) {
      // Set auth token
      user = createdUser
      done()
    }, done)
    .catch(function (err) {
      console.log("ERROR: ", err)
    })
  })

  // Clear users after testing
  after(function () {
    utils.dropDB([Forecast])
  })

  authTest({
    name: 'Users',
    models: [User],
    endpoints: [
      {
        description: 'GET /api/users',
        req: function () {
          return request(app).get('/api/users')
        }
      }
    ]
  })

  // # `As a user, I want to be redirected to a pretty error page if the route does not exist`
  // * 1 - `Modify existing error page to React instead EJS and redirect to nice error page when the user gets a 404`
  // * 2 - `Fix related tests if applicable`
  describe('As a user, I want to be redirected to a pretty error page if the route does not exist', function () {

    xit('should return 404 when hit /nonexistent endpoint', function (done) {

      request(app)
      .get('/notexistent')
      .expect(404).end(function (err, res) {
        done()
      })
    })
  })


//   # `As a user, I want to enter my user name and password, so I can log in`
//   ## `Currently we use a combination of phone number and password. However During the previous feedback session our customer indicated that they prefer to login with user name and password.`
//   * 1 - `Modify existing login page to use React instead EJS`
//   * 2 - `Create a new authorization strategy to use username and password`
//   * 3 - `Fix related tests if applicable`
  describe('As a user, I want to enter my user name and password, so I can log in', function () {

    //'should login with a username and password'
    xit('Should login with a username and password ', function (done) {

    })

  })

  // # `As a user, I want to receive a unique code in my email, so I can log in a more securely`
  // * 1 - `Implement login two-factor auth strategy defined on /auth/authroute to send an email to the user with a code`
  // * 2 - `Allow the user to choose between username/password or email/passcode to login`
  // * 3 - `Fix related tests if applicable`
  describe('As a user, I want to receive a unique code in my email, so I can log in a more securely', function () {

    xit('should send email when login securely', function (done) {


    })
  })

  // # `As a Meterelogist, I want to manage weather information, so I can keep our users with acurate information all times`
  // * 1 - `Add forecasts into the application, you can start by looking to the example tasks`
  // * 2 - `Model: Forecasts contain a station_id, place_name, latitude, longitude, datetime, temperature_max,temperature_min, precipitation_probability and precipitation_mm ... full JSON -> `
  //   `{
  //    "station_id":1438,
  //    "place_name":"Amsterdam",
  //    "latitude":52.3,
  //    "longitude":4.766667,
  //    "datetime":"2014-08-08 00:00:00",
  //    "temperature_max":"24.2",
  //    "temperature_min":"15.1",
  //    "precipitation_probability":"90",
  //    "precipitation_mm":"6.0"
  // }`
  // * 3 - `Management: Users should be able to create, complete, edit and delete their own forecasts`
  // * 4 - `Pagination: forecasts should be shown 10 at a time`
  // * 5 - `Fix related tests if applicable`
  describe('As a Meterelogist, I want to manage weather information, so I can keep our users with acurate information all times', function () {

    xit('should [Add] forecasts to the application', function (done) {

    })

    xit('should [Add] HTTP forecasts to the application', function (done) {

    })


    xit('should [Model] Forecasts contain a user, name, description, startDate, endDate, completed', function (done) {

    })
    xit('should [Management] Users should be able to create, complete, edit and delete their own forecasts', function (done) {

    })
    xit('should [Pagination] Forecasts should be shown 10 at a time', function (done) {

    })

    // # `As a user, I want to sort my forecasts, so I can have a better overview`
    //     * 1 - `Sort forecast based on name, dates, status`
    //     * 2 - `Fix related tests if applicable`
    describe('As a user, I can sort my forecasts, so I can have a better overview', function () {
      xit('should Sort forecast based on name, dates, status', function (done) {

      })

      // # ` As a user, I want to be able to filter forecasts`
      // * 1 - `Enable filtering by datetime`
      // * 2 - `Enable filtering by words that match place_name`
      // * 3 - `Fix related tests if applicable`
      describe('As a user, I want to be able to filter forecasts', function () {

        xit('should Enable filtering by date', function (done) {

        })

        xit('should Enable filtering by words that match name and description', function (done) {

        })

      })


      // # `As an integrator, I want to have access to a webhook, so I can integrate my 'Service'`
      // # `The service we're integrating tells our server that a forecast has been changed remotely`
      // * 1 - `It should accept the fields uid and tid and status`
      // * 2 - `It should check whether the user has the specified forecast and increment a change counter`
      // * 3 - `It should return 200 OK`
      // * 4 - `Fix related tests if applicable`
      describe('As an integrator, I want to have access to a webhook, so I can integrate my Service', function () {

        xit('should should check whether the user has the specified forecast and increment a change counter', function (done) {

        })

      })

    })

  })
})
