'use strict'
let app = require('../app')

let Promise = require('bluebird')
let request = require('supertest')

let Model = require('../../lib/model')
let User = Model.User
let Chance = require('chance')
let utils = require('../utils/api-utils')

// -- UTIL METHODS

// Random user id
const ID = '000000000000000000000000'

function generateId() {
    let chance = new Chance()
    return chance.hash({length: 24})
}

/* Creates a user and returns a token that we can use to log in */
function createUser() {
    let deferred = defer();
    let userData = User._mocks.getMinimal({apnDeviceToken: 'random'})
    userData.phoneNumber = utils.parsePhoneNumber(userData.phoneNumber)
    new User(userData)
        .save()
        .then(function () {
            request(app)
                .post('/auth/local')
                .send(userData)
                .expect(200)
                .expect('Content-Type', /json/)
                .end(function (err, res) {
                    // Promise part
                    if (err) {
                        deferred.reject(err)
                    }
                    let token = res.body.token
                    deferred.resolve(token)
                })
        })

    return deferred.promise
}

/**
 * Remove all models from the DB
 * @param {*} done callback
 * @param models
 * @returns {*} promise
 */
function dropDB(models) {
    return Promise.map(models, model => model ? model.remove() : true)
}

/**
 * Reinitialize the DB and add 1 user
 * @param models
 * @returns {*} promise
 */ // FIXME implement otherwise
function resetDB(models) {
    return dropDB(models).then(() => createUser())
}

function clearDB(models) {
    return dropDB(models)
}

function defer() {
    var resolve, reject;
    var promise = new Promise(function () {
        resolve = arguments[0];
        reject = arguments[1];
    });
    return {
        resolve: resolve,
        reject: reject,
        promise: promise
    };
}

exports.ID = ID
exports.generateId = generateId
exports.createUser = createUser
exports.dropDB = dropDB
exports.resetDB = resetDB
exports.clearDB = clearDB
