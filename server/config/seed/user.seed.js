'use strict'
let Model = require('model')
let User = Model.User

let userWeekDaySchedule = { d: [2, 3, 4, 5, 6], h: [8, 9, 10, 11, 12, 13, 14, 15, 16] }
let schedule = {schedules: userWeekDaySchedule, exceptions: []}

User.find({}).remove(function () {
  User.create(
    {
      _id: 'aaabbbcccdddeeefff000001',
      provider: 'local',
      firstName: 'Test User',
      email: 'test@test.com',
      phoneNumber: '003212345678',
      password: 'test1234'
    },
    {
      _id: 'aaabbbcccdddeeefff000002',
      provider: 'local',
      firstName: 'Test User Two',
      email: 'test@test.com',
      phoneNumber: '003212555555',
      password: 'test1234'
    },
    {
      _id: 'aaabbbcccdddeeefff000003',
      provider: 'local',
      firstName: 'Test User Three',
      email: 'test@test.com',
      phoneNumber: '003212777777',
      password: 'test1234'
    },
    {
      _id: 'aaabbbcccdddeeefff000004',
      provider: 'local',
      role: 'admin',
      firstName: 'Admin',
      email: 'admin@admin.com',
      phoneNumber: '003212345679',
      password: 'admin1234'
    },
    {
      _id: 'aaabbbcccdddeeefff000005',
      provider: 'local',
      role: 'admin',
      firstName: 'Test 1',
      email: 'testg@test123.org',
      phoneNumber: '32485009802',
      password: 'admin1234'
    },
    {
      _id: 'aaabbbcccdddeeefff000006',
      provider: 'local',
      role: 'admin',
      firstName: 'Jef',
      email: 'adg@das.io',
      phoneNumber: '32477238435',
      password: 'admin1234'
    },
    {
      _id: 'aaabbbcccdddeeefff000008',
      provider: 'local',
      role: 'admin',
      firstName: 'asa',
      email: 'sven@asa.io',
      phoneNumber: '32486939376',
      password: 'admin1234'
    },
    {
      schedule,
      role: 'admin',
      powers: { canWork: true },
      _id: 'aaabbbcccdddeeefff000009',
      provider: 'local',
      firstName: 'sasa',
      email: 'me@asa.com',
      phoneNumber: '32477305228',
      password: 'admin1234'
    },
    {
      schedule,
      role: 'admin',
      powers: { canWork: true },
      _id: 'aaabbbcccdddeeefff000010',
      provider: 'local',
      firstName: 'asas',
      email: 'qwe@Fullstack Assessment Master',
      phoneNumber: '31619955777',
      password: 'admin1234'
    },
    {
      schedule,
      role: 'admin',
      powers: { canWork: true },
      _id: 'aaabbbcccdddeeefff000011',
      provider: 'local',
      firstName: 'qwe.ua',
      email: 'pavlo@bescow.com',
      phoneNumber: '380505911466',
      password: 'admin1234'
    },
    {
      schedule,
      role: 'admin',
      powers: { canWork: true },
      _id: 'aaabbbcccdddeeefff000012',
      provider: 'local',
      firstName: 'qwe.ua',
      email: 'qwe@qwe.com',
      phoneNumber: '380983231444',
      password: 'admin1234'
    },

    function (err, res) {
      if (err) {
        throw err
      }
      console.log('finished populating users')
    }
  )
})
