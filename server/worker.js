/**
 * Main application file
 */

'use strict'

// Set default node environment to development
process.env.NODE_ENV = process.env.NODE_ENV || 'development'

require('logger')
let config = require('config')

let mongoose = require('mongoose')

// Connect to MongoDB
mongoose.connect(config.mongo.uri + '/' + config.mongo.db, config.mongo.options)
mongoose.connection.on('error', function (err) {
  console.error('MongoDB connection error: ' + err)
  process.exit(-1)
})

// setInterval(require('./jobs/schedule.job'),60000)
// let Example = require('./jobs/example.job')
// setInterval(new Example().start, 30 * 1000)
