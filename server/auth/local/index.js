'use strict'

let express = require('express')
let passport = require('passport')
let auth = require('../auth.service')

let router = express.Router()

/**
 * @api {post} /auth/local authorize
 * @apiVersion 1.0.0
 * @apiName Auth
 * @apiGroup Auth Local
 *
 * @apiDescription Authorize user with username and password
 *
 * @apiParam {String} username
 * @apiParam {String} password
 *
 * @apiExample Example usage:
 * curl -xPOST -i http://localhost/auth/local -d {
 * 		username: 'admin',
 * 		password: 'admin'
 * }
 *
 * @apiSuccess {String} token
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *     		token: 'oiajfoi3j4oijoi24nekj3n2kn4kjn32kjnkkekw'
 *     }
 * @apiErrorExample {text} Error-Response:
 * 		HTTP/1.1 401 UNAUTHORIZED
 *
 * @apiErrorExample {text} Error-Response:
 * 		HTTP/1.1 404 NOT FOUND
 *
 */
router.post('/', function (req, res, next) {
  passport.authenticate('local', function (err, user, info) {
    let error = err || info
    if (error) {
      console.log("Error: passport.authenticate, ",error);
      return res.status(401).json(error)
    }
    if (!user) {
      return res.status(404).json({message: 'Something went wrong, please try again.'})
    }

    let token = auth.signToken(user._id, user.role)
    res.json({ token: token })
  })(req, res, next)
})

module.exports = router
