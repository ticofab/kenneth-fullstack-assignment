'use strict'

let config = require('../../lib/config')
let jwt = require('jsonwebtoken')
let expressJwt = require('express-jwt')
let compose = require('composable-middleware')

let Model = require('../../lib/model')
let User = Model.User

let validateJwt = expressJwt({
  secret: config.secrets.session
})

/**
 * Attaches the user object to the request if authenticated
 * Otherwise returns 403
 */
function isAuthenticated () {
  return compose()
    // Validate jwt
    .use(function handleJwt (req, res, next) {
      // allow access_token to be passed through query parameter as well
      if (req.query.access_token) {
        req.headers.authorization = 'Bearer ' + req.query.access_token
        delete req.query.access_token
      }

      // Validates token and sets user object on request
      validateJwt(req, res, next)
    })
    // Attach current user data to request
    .use(function attachUser (req, res, next) {
      return User
      .findByIdAndUpdate(req.user._id, {lastLoginAt: Date.now()})
      .then(function handleUser (user) {
        if (!user) {
          res.status(401).end()
          return null  // Return null statements silent any Bluebird warnings
        }

        req.user = user
        next()
        return null // Return null statements silent any Bluebird warnings
      }, next)
    })
}

/**
 * Checks if the user role meets the minimum requirements of the route
 */
function hasRole (roleRequired) {
  if (!roleRequired) {
    throw new Error('Required role needs to be set')
  }

  return compose()
    .use(isAuthenticated())
    .use(function meetsRequirements (req, res, next) {
      if (config.userRoles.indexOf(req.user.role) >=
          config.userRoles.indexOf(roleRequired)) {
        next()
      } else {
        res.status(403).send('Forbidden')
      }
    })
}

/**
 * Returns a jwt token signed by the app secret
 */
function signToken (id, role) {
  return jwt.sign({ _id: id, role: role }, config.secrets.session, {
    // TODO check for refreshing tokens
    expiresIn: '1000 days'
  })
}

/**
 * Set token cookie directly for oAuth strategies
 */
function setTokenCookie (req, res) {
  if (!req.user) {
    return res.json(404, { message: 'Something went wrong, please try again.' })
  }
  let token = signToken(req.user.id, req.user.role)
  res.cookie('token', token)
  res.redirect('/')
}

exports.isAuthenticated = isAuthenticated
exports.hasRole = hasRole
exports.signToken = signToken
exports.setTokenCookie = setTokenCookie
