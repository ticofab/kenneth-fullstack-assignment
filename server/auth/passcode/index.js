'use strict'

let Model = require('model')
let i18n = require('i18n')

let utils = require('../../utils/api-utils')

let express = require('express')
let passport = require('passport')
let auth = require('../auth.service')

let router = express.Router()

/**
 * @api {post} /auth/passcode authorize
 * @apiVersion 1.0.0
 * @apiName Auth
 * @apiGroup Auth  passcode
 *
 * @apiDescription Authorize user with a email and a passcode
 *
 * @apiParam {String} email
 * @apiParam {String} passcode
 *
 * @apiExample Example usage:
 * curl -xPOST -i http://localhost/auth/passcode -d {
 * 		email: 'test@test.com',
 * 		passcode: '6230'
 * }
 *
 * @apiSuccess {String} token
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *     		token: 'oiajfoi3j4oijoi24nekj3n2kn4kjn32kjnkkekw'
 *     }
 *
 * @apiErrorExample {text} Error-Response:
 * 		HTTP/1.1 401 UNAUTHORIZED
 *
 * @apiErrorExample {text} Error-Response:
 * 		HTTP/1.1 404 NOT FOUND
 *
 */
router.post('/', function (req, res, next) {

})

module.exports = router
