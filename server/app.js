/**
 * Main application file
 */

'use strict'

// Set default node environment to development
process.env.NODE_ENV = process.env.NODE_ENV || 'development'

// Hook the logger into the main application
require('logger')

let config = require('../lib/config')

let express = require('express')
let mongoose = require('mongoose')
mongoose.Promise = require('bluebird')

let http = require('http')

// Connect to MongoDB
mongoose.connect(config.mongo.uri + '/' + config.mongo.db, config.mongo.options)
mongoose.connection.on('error', function (err) {
  console.error('MongoDB connection error: ' + err)
  process.exit(-1)
})

// Populate DB with sample data
if (config.seedDB === 'true') { require('./config/seed') }

// Setup server
let app = express()
let server = http.createServer(app)
require('./config/express')(app)

require('./route')(app)

function handleError (error, prevResponse) {
  let errorResponse = prevResponse || {
    name: error.name,
    message: error.message,
    errors: []
  }

  // Handle mongoose validation errors
  if (error.name === 'ValidationError') {
    error.status = 422
    for (let err in error.errors) {
      if (error.errors.hasOwnProperty(err)) {
        let errorData = error.errors[err]
        errorResponse.errors.push({
          message: errorData.message,
          name: errorData.name,
          path: errorData.path,
          value: errorData.value
        })
      }
    }
  }

  if (error.next) {
    return handleError(error.next, errorResponse)
  }

  return errorResponse
}

// Error Handler must be last
let env = app.get('env')
if (env === 'production') {
  //TODO: do not return full errs in PROD
  app.use(function (err, req, res, next) {
    console.error(err)
    let errorResponse = handleError(err)
    console.error(errorResponse)
    return res.status(err.status || 500).json(errorResponse)
  })
} else {
  app.use(function (err, req, res, next) {
    console.error(err)
    let errorResponse = handleError(err)
    console.error(errorResponse)
    return res.status(err.status || 500).json(errorResponse)
  })
}

// Start server
server.listen(config.port, config.ip, function () {
  console.log('Express server listening on %d, in %s mode', config.port, app.get('env'))
})

// Expose app
exports = module.exports = app
