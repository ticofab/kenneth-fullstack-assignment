'use strict'

let express = require('express')
let controller = {
  User: require('../user/user.controller')
}

let auth = require('../../auth/auth.service')

let router = express.Router()

router.get('/', auth.isAuthenticated(), controller.User.me)

router.put('/', auth.isAuthenticated(), controller.User.updateMyself)

module.exports = router
