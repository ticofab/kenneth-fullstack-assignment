'use strict'

let request = require('supertest')
let should = require('should')

let app = require('../../app')

let utils = require('../../test/utils')

let Model = require('../../../lib/model')
let User = Model.User

let UserMocks = User._mocks

// let SECURITY_FIELDS = ['hashedPassword', 'salt']
let Chance = require('chance')
let chance = new Chance()

describe('User API: ', function () {
    let user
    let token
    let appleToken = chance.apple_token()
    let gender = chance.gender()
    let first = chance.first({gender: gender})
    let last = chance.last({gender: gender})
    let email = [first, '.', last, '@', chance.domain()].join('')
    let role = 'admin'
    let isOperator = true

    let testUser = {
        _id: chance.hash({length: 24}),
        phoneNumber: '32485' + chance.string({length: 6, pool: '0123456789'}),
        powers: { canWork: true },
        password: chance.hash(),
        apnDeviceToken: appleToken,
        gender: gender,
        firstName: first,
        lastName: last,
        email: email,
        schedule: {
            schedules: [{
                d: [2, 3, 4, 5, 6],
                h: [9, 10, 11, 12, 14, 15, 16, 17]
            }]
        },
        role: role,
        isOperator: isOperator
    }

    // Clear users before testing
    before(function (done) {
        utils
            .resetDB([User])
            .then(function (userToken) {
                // Set auth token
                token = userToken
                return User.create(testUser)
            })
            .then(function (createdUser) {
                // Set auth token
                user = createdUser
                done()
            }, done)
            .catch(function (err) {
                console.log("ERROR: ", err)
            })
    })

    // Clear users after testing
    after(function () {
        utils.dropDB([User])
    })

    describe('GET /api/users/:id', function () {
        // FIXME add user data to verify the correct one is returned
        xit('should respond with a user profile when authenticated', function (done) {
            request(app)
                .get('/api/users/' + user.id)
                .set('authorization', 'Bearer ' + token)
                .expect(200)
                .expect('Content-Type', /json/)
                .end(function (err, res) {
                    if (err) {
                        return done(err)
                    }

                    should.exist(res.body)
                    res.body.should.have.properties({
                        'firstName': first,
                        'lastName': last,
                        'role': role,
                        'isOperator': isOperator
                    })

                    done()
                })
        })

        xit('should respond with a 404 if the user does not exist', function (done) {
            request(app)
                .get('/api/users/' + utils.ID)
                .set('authorization', 'Bearer ' + token)
                .expect(404)
                .expect('Content-Type', /json/)
                .end(function (err, res) {
                    if (err) {
                        return done(err)
                    }

                    done()
                })
        })
    })

    describe('GET /api/users/me', function () {
        // FIXME add user data to verify the correct one is returned
        xit('should respond with a user profile when authenticated', function (done) {
            request(app)
                .get('/api/users/me')
                .set('authorization', 'Bearer ' + token)
                .expect(200)
                .expect('Content-Type', /json/)
                .end(function (err, res) {
                    if (err) {
                        return done(err)
                    }

                    should.exist(res.body._id)
                    done()
                })
        })
    })

    describe('PUT /api/users/toggleOperatorMode', function () {
        // FIXME add user data to verify the correct one is returned
        xit('should respond with a toggled operator status when authenticated', function (done) {
            request(app)
                .put('/api/users/toggleOperatorMode')
                .set('authorization', 'Bearer ' + token)
                .expect(200)
                .expect('Content-Type', /json/)
                .end(function (err, res) {
                    if (err) {
                        return done(err)
                    }

                    should.exist(res.body.isOperator)
                    res.body.should.have.properties({isOperator: true})
                    done()
                })
        })
    })

    describe('PUT /api/users/', function () {
        // FIXME add user data to verify the correct one is returned
        xit('should fail when no password is given', function (done) {
            request(app)
                .put('/api/users/')
                .send({phoneNumer: '13456782354'})
                .expect(422)
                .end(function (err, res) {
                    if (err) {
                        return done(err)
                    }

                    done()
                })
        })

        xit('should fail when no phoneNumber is given', function (done) {
            request(app)
                .put('/api/users/')
                .send({password: 'hodor1234'})
                .expect(422)
                .end(function (err, res) {
                    if (err) {
                        return done(err)
                    }

                    done()
                })
        })

        xit('should fail when a password shorter than 6 characters is given', function (done) {
            request(app)
                .put('/api/users/')
                .send({phoneNumber: '12345', password: 'hodor'})
                .expect(422)
                .end(function (err, res) {
                    if (err) {
                        return done(err)
                    }

                    done()
                })
        })

        xit('should return a token', function (done) {
            request(app)
                .put('/api/users/')
                .send(UserMocks.getMinimal())
                .expect(200)
                .end(function (err, res) {
                    if (err) {
                        return done(err)
                    }
                    res.body.should.have.property('token')
                    done()
                })
        })
    })

    describe('PUT /api/users/:id', function () {
        let userData = {firstName: 'hodor'}
        // FIXME add user data to verify the correct one is returned
        xit('should respond with a updated user', function (done) {
            request(app)
                .put('/api/users/' + user.id)
                .set('authorization', 'Bearer ' + token)
                .send(userData)
                .expect(200)
                .end(function (err, res) {
                    if (err) {
                        return done(err)
                    }

                    should.exist(res.body)
                    should.exist(res.body._id)
                    done()
                })
        })

        xit('should have updated the user', function (done) {
            User
                .find(userData)
                .then(function (user) {
                    should.exist(user)
                    user.should.be.instanceof(Array).and.have.lengthOf(1)
                    done()
                })
        })
    })

    describe('PUT /api/users/me', function () {
        // FIXME add user data to verify the correct one is returned
        xit('should respond with 204', function (done) {
            request(app)
                .put('/api/users/me')
                .set('authorization', 'Bearer ' + token)
                // FIXME password should never be updated this way
                .send({firstName: 'hodor', password: 'test123456'})
                .expect(200)
                .end(function (err, res) {
                    if (err) {
                        return done(err)
                    }

                    should.exist(res.body)
                    should.exist(res.body._id)

                    done()
                })
        })

        xit('should have updated the user', function (done) {
            request(app)
                .get('/api/users/me')
                .set('authorization', 'Bearer ' + token)
                .expect(200)
                .end(function (err, res) {
                    if (err) {
                        return done(err)
                    }

                    should.exist(res.body)
                    res.body.should.have.properties({firstName: 'hodor'})
                    done()
                })
        })
    })

})
