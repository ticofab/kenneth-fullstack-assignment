'use strict'

let Errors = require('errors')
let _ = require('underscore')
let uid = require('uid')

let Model = require('../../../lib/model')
let User = Model.User

// Salt and hashedPassword should never be returned
const PRIVATE_FIELDS = '-salt -password'

class UserService {
  constructor () {}

  find (queryObject) {
    return User.find(queryObject || {}, PRIVATE_FIELDS)
  }

  findById (userId) {
    return User
      .findById(userId, PRIVATE_FIELDS)
      .then(function (user) {
        return user
      })
  }

  create (userData, createPassword) {
    let newUser = new User(userData)
    newUser.provider = 'local'
    newUser.role = 'user'

    // Generate a default password for new users
    if (createPassword) {
      newUser.password = newUser.password || uid()
    }
    return newUser
      .save()
      .then(function (user, numAffected) {
        return user
      })
      /*
      .catch(function (err) {
        throw new Errors.Validation(err.message)
      })
      */
  }

  createWithPassword (userData) {
    return this.create(userData, true)
  }

  update (id, userData) {
    return User
      .findById(id)
      .then(function (user) {
        _.assign(user, userData)
        // Need to explicitely save in order to use validation
        return user.save()
      })
      .then(function (user) {
        return user
      })
  }

  safeUpdate (id, userData) {
    // Don't allow updates on secure fields
    userData = _.omit(userData, ['password', 'salt', 'id', '_id'])
    return this.update(id, userData)
  }

  destroy (userId) {
    return User.findByIdAndremove(userId)
  }

  changePassword (userId, oldPass, newPass) {
    return User
      .findById(userId)
      .then(function (user) {
        if (user.authenticate(oldPass)) {
          user.password = newPass
          return user.save()
        } else {
          throw new Errors[403]('Invalid password')
        }
      })
  }
}

module.exports = new UserService()
