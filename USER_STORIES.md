Hello there developer. We need to build a new application that lets Meteorologists manage & view weather information. 
Keep in mind that our users are quite demanding in terms of snappyness: any long wait or timeout will make them leave our service forever. 
Thats why we want to refactor and boost this little app into React with server side rendering.

Our Product Owner prepared a set of user stories that we need to get done in order to be able to satisfy our customer needs.

Each user story have a timebox of 1h, some can take longer, some can take less, but try to keep this as close as possible to our expectations.

We've set up a basic node app to get you started, we hope this will get you going asap.

Here are the user stories the Product Owner said we must have ready for the demo:

# `As a user, I want to enter my user name and password, so I can log in`
## `Currently we EJS as basis of our FE logic. However During the previous feedback session our customer indicated that they prefer to have it with React.`
* 1 - `Modify existing login page to use React instead EJS`
* 2 - `Existing authorization strategy for username and password already works, make sure it redirects to the dashboard after user is logged in`
* 3 - `Fix related tests if applicable`

# `As a user, I want to be redirected to a pretty error page if the route does not exist`
* 1 - `Modify existing error page to React instead of EJS and redirect to nice error page when the user gets a 404`
* 2 - `Fix related tests if applicable`

# `As a user, I want to receive a unique code in my email, so I can log in a more securely`
* 1 - `Modify existing page to React instead of EJS to login two-factor auth strategy defined on /auth/authroute to send an email to the user with a code`
* 2 - `Allow the user to choose between username/password or email/passcode to login`
* 3 - `Fix related tests if applicable`

# `As a Meterelogist, I want to manage weather information, so I can keep our users with acurate information all times`
* 1 - `Modify existing page to React instead of EJS and Add forecasts into the application, you can start by looking to the example tasks`
* 2 - `Model: Forecasts contain a station_id, place_name, latitude, longitude, datetime, temperature_max,temperature_min, precipitation_probability and precipitation_mm ... full JSON -> ` 
  `{
     "station_id":1438,
     "place_name":"Amsterdam",
     "latitude":52.3,
     "longitude":4.766667,
     "datetime":"2014-08-08 00:00:00",
     "temperature_max":"24.2",
     "temperature_min":"15.1",
     "precipitation_probability":"90",
     "precipitation_mm":"6.0"
  }`
* 3 - `Management: Users should be able to create, complete, edit and delete their own forecasts`
* 4 - `Pagination: forecasts should be shown 10 at a time`
* 5 - `Fix related tests if applicable`

# `As a user, I want to sort my forecasts, so I can have a better overview`
* 1 - `Modify existing page to React instead of EJS & Sort forecasts based on name, dates, temperature(min/max), and preciopitation `
* 2 - `Fix related tests if applicable`

# ` As a user, I want to be able to filter forecasts`
* 1 - `Modify existing page to React instead of EJS & Enable filtering by datetime`
* 2 - `Modify existing page to React instead of EJS & Enable filtering by words that match place_name`
* 3 - `Fix related tests if applicable`

# `As an integrator, I want to have access to a webhook, so I can integrate my 'Service'`
# `The service we're integrating tells our server that a forecast has been changed remotely`
* 1 - `It should accept the fields uid and tid and status`
* 2 - `It should check whether the user has the specified weather and increment a change counter`
* 3 - `It should return 200 OK`
* 4 - `Fix related tests if applicable`

# Non functional requirements:
* 1 - All mandatory integration tests need to pass `npm run test`
* 2 - Fix/update specs that are currently not passing 
* `node_modules/.bin/mocha server/test/utils.spec.js`
* `node_modules/.bin/mocha server/api/user/user.spec.auth.js`
* `node_modules/.bin/mocha lib/model/models/authCode/authcode.model.spec.js`
* `node_modules/.bin/mocha lib/model/models/user/user.model.spec.js `
