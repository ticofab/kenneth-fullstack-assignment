'use strict'
require('logger')
let path = require('path')
let fs = require('fs')

class Services {
  constructor () {
    console.debug(process.env)
    console.debug('Initializing services')

    let normalizedPath = path.join(__dirname, 'services')

    let _this = this
    fs.readdirSync(normalizedPath).forEach(function (file) {
      file = file.split('.')[0]
      let serviceName = file.charAt(0).toUpperCase() + file.slice(1)
      let servicePath = path.join(normalizedPath, file)

      _this[serviceName] = require(servicePath)
      _this[serviceName + 'Service'] = _this[serviceName]
    })
  }
}

module.exports = exports = new Services()
