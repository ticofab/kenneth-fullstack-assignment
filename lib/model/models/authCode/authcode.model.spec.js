'use strict'
let should = require('should')
let Model = require('../../index')

let AuthCode = Model.AuthCode
let AuthCodeMock = AuthCode._mocks

describe('AuthCode Model: ', function () {
  describe('creation ', function () {
    let tests = [{}, {duration: 1}]
    tests.forEach(function (data) {
      xit('should fail if required fields are not set', function (done) {
        AuthCode
        .create(data)
        .then(function (result) {
          should.not.exist(result)
          done()
        })
        .catch(function (err) {
          should.exist(err)
          err.name.should.eql('ValidationError')
          done()
        })
        .catch(done)
      })
    })

    xit('should succeed saving a minimal mock object', function (done) {
      let data = AuthCodeMock.getMinimal()
      AuthCode.create(data).then(function (result) {
        should.exist(result)
        done()
      }).catch(done)
    })

    xit('should succeed saving a full blown mock object', function (done) {
      let data = AuthCodeMock.get()
      AuthCode.create(data).then(function (result) {
        should.exist(result)
        done()
      }).catch(done)
    })

    let badInput = ['a', 'abcd', 'abcde', 1, 12345, 'aa11', '11aa', '123', '1', [], {}, null, undefined]
    badInput.forEach(function (data) {
      data = AuthCodeMock.getMinimal({passcode: data})
      xit('should fail if code is not 4 digits long', function (done) {
        AuthCode
        .create(data)
        .then(function (result) {
          should.not.exist(result)
          done()
        })
        .catch(function (err) {
          should.exist(err)
          err.name.should.eql('ValidationError')
          done()
        })
        .catch(done)
      })
    })

    let badPhoneNumbers = ['hodor', '1', '123', null, undefined]
    badPhoneNumbers.forEach(function (data) {
      xit('should fail if phoneNumber is invalid', function (done) {
        data = AuthCodeMock.getMinimal({phoneNumber: data})
        AuthCode
        .create(data)
        .then(function (code) {
          should.not.exist(code)
          done()
        })
        .catch(function (err) {
          should.exist(err)
          console.error(err)
          err.name.should.eql('ValidationError')
          done()
        })
        .catch(done)
      })
    })

    xit('should fail if a auth code with the same phoneNumber exists', function (done) {
      let data = {phoneNumber: '31123456789', passcode: '1234'}
      AuthCode.create(data).then(function () {
        return AuthCode.create(data)
      })
      .then(function (result) {
        should.not.exist(result)
        done()
      }, function (err) {
        should.exist(err)
        err.name.should.eql('ValidationError')
        done()
      })
      .catch(done)
    })
  })
})
