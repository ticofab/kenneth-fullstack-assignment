'use strict'

let Config = require('config')

let crypto = require('crypto')
let mongoose = require('mongoose')// require('bluebird').promisifyAll(require('mongoose'))
let Schema = mongoose.Schema

let authTypes = ['github', 'twitter', 'facebook', 'google', 'twoFactor']

let UserSchema = new Schema({
    deviceId: String,
    phoneNumber: {
        type: String,
        lowercase: true,
        required: true,
        index: true
        // TODO add match for validation
    },
    email: {
        type: String,
        lowercase: true,
        index: true
    },
    isOperator: {
        // Indicates if the user is active as an operator NOW */
        type: Boolean,
        default: false
    },
    powers: { // those are set by Admin with Admin app
        canOrder: {
            type: Boolean,
            default: false
        },
        canWork: {
            type: Boolean,
            default: false
        },
        canOperate: {
            type: Boolean,
            default: false
        }
    },
    ratingByClient: {type: Number, default: 0},
    ratingByClientCount: {type: Number, default: 0},
    ratingByWorker: {type: Number, default: 0},
    ratingByWorkerCount: {type: Number, default: 0},

    address: {
        postcode: {type: String, default: '2514 GL'},
        houseNumber: {type: String, default: '68'},
        houseNumberAddition: {type: String, default: ''},
        street: {type: String, default: 'Noordeinde'},
        municipality: String,
        province: String,
        latitude: String,
        longitude: String,
        city: {type: String, default: 'Den Haag'},
        country: {type: String, default: 'Nederland'}
    },

    firstName: String,
    lastName: String,

    // Scheduling
    schedule: {
        schedules: [{
            d: [Number], // Day of the week
            h: [Number], // At specific hours
            from: Date, // For a specific period
            to: Date    // Until a psecific period
        }],
        exceptions: [{
            jobId: String,
            d: [Number],
            h: [Number],
            wy: [Number],
            m: [Number],
            from: Date,
            to: Date
        }]
    },
    scheduleExceptions: [{from: Date, to: Date}],

    // Push notification Stuff
    apnDeviceToken: String,
    receiveNotifications: {type: Boolean, default: true},
    // Event info
    createdAt: {type: Date, default: Date.now},
    lastLoginAt: Date,

    // Auth Stuff
    role: {
        type: String,
        default: 'user'
    },
    password: {type: String},
    username: {type: String},
    provider: String,
    salt: String,
    facebook: {},
    twitter: {},
    google: {},
    billingDetails: {
        address: {
            postcode: {type: String},
            houseNumber: {type: String},
            houseNumberAddition: {type: String, default: ''},
            street: {type: String},
            municipality: String,
            province: String,
            city: {type: String},
            country: {type: String}
        },
        person: {
            firstName: String,
            lastName: String
        }
    },
    coupons: [{
        value: Number,
        issuedBy: {type: String, ref: 'User'},
        occasion: {type: String, ref: 'Issue'},
        disposedFor: {type: String, ref: 'Invoice'}
    }]
})

/**
 * Virtuals
 */

// Public profile information
UserSchema
    .virtual('profile')
    .get(function () {
        return {
            'firstName': this.firstName,
            'lastName': this.lastName,
            'role': this.role,
            'isOperator': this.isOperator
        }
    })

// Non-sensitive info we'll be putting in the token
UserSchema
    .virtual('token')
    .get(function () {
        return {
            '_id': this._id,
            'role': this.role
        }
    })

/**
 * Validations
 */
//
// Validate empty email
UserSchema
    .path('email')
    .validate(function (email) {
        if (authTypes.indexOf(this.provider) !== -1) {
            return true
        }
        return email.length
    }, 'Email cannot be blank')

function hasNumbers(str) {
    return /\d/.test(str)
}

function textOnlyValidator(str, respond) {
    if (str) {
        respond(!hasNumbers(str), '{PATH} cannot contain numbers')
    }
    respond(true)
}

UserSchema.path('firstName').validate(textOnlyValidator)
UserSchema.path('lastName').validate(textOnlyValidator)

// Validate empty phoneNumber
let phone = require('node-phonenumber')
let phoneUtil = phone.PhoneNumberUtil.getInstance()
UserSchema
    .path('phoneNumber')
    .validate(function (phoneNumber) {
        /**
         if (authTypes.indexOf(this.provider) !== -1) {
      return true
    }**/
        // TODO Add simple phone number regex
        if (!phoneNumber) {
            return false
        }
        let phoneNr
        try {
            phoneNr = phoneUtil.parse('+' + phoneNumber, 'NL')
        } catch (err) {
            return false
        }

        return phoneUtil.isValidNumber(phoneNr)
    }, '{PATH} must be valid NL phone number')

// Validate empty password
// UserSchema
//     .path('password')
//     .validate(function (password) {
//         if (authTypes.indexOf(this.provider) !== -1) {
//             return true
//         }
//         return password.length <= 1
//     }, 'Password must be 4 chars length')

// Validate empty username
// UserSchema
//     .path('username')
//     .validate(function (username) {
//         if (authTypes.indexOf(this.provider) !== -1) {
//             return true
//         }
//         return username.length >= 0
//     }, 'Username cannot be blank')


/* UNIQUE EMAIL IS NOT A CONSTRAINT (YET)
 // Validate email is not taken
 UserSchema
 .path('email')
 .validate(function (value, respond) {
 let self = this
 return this.constructor.findOne({ email: value })
 .then(function (user) {
 if (user) {
 if (self.id === user.id) {
 return respond(true)
 }
 return respond(false)
 }
 return respond(true)
 })
 .catch(function (err) {
 throw err
 })
 }, 'The specified email address is already in use.')
 */

// Validate phoneNumber is not taken
UserSchema
    .path('phoneNumber')
    .validate(function (value, respond) {
        let self = this;
        this.constructor.findOne({phoneNumber: value})
            .then(function (user) {
                return (user ? (self.id === user.id) : true)
            })
            .then(respond);
    }, 'The specified phone number is already in use.');

let validatePresenceOf = function (value) {
    return value && value.length
}

/**
 * Pre-save hook
 */
UserSchema
    .pre('save', function (next) {
        // Handle new/update passwords
        if (this.isModified('password')) {
            if (!validatePresenceOf(this.password) &&
                authTypes.indexOf(this.provider) === -1) {
                next(new Error('Invalid password'))
            }

            // Make salt with a callback
            let _this = this
            this.makeSalt(function (saltErr, salt) {
                if (saltErr) {
                    next(saltErr)
                }
                _this.salt = salt
                _this.encryptPassword(_this.password, function (encryptErr, hashedPassword) {
                    if (encryptErr) {
                        next(encryptErr)
                    }
                    _this.password = hashedPassword
                    next()
                })
            })
        } else {
            next()
        }
    })
// TODO implement later
UserSchema.pre('save', function (next) {
    if (!this.isModified('address')) {
        return next()
    }
    if (!this.address || !this.address.postcode || !this.address.houseNumber) {
        return next()
    }

    // Trim house number and postcode
    this.address.postcode = this.address.postcode.trim()
    this.address.houseNumber = this.address.houseNumber.trim()

    console.debug('[User Model] Attempting to enrich address data')
    console.debug(this.address)

    let key = Config.postcode.key
    let secret = Config.postcode.secret
    let auth = 'Basic ' + new Buffer(`${key}:${secret}`).toString('base64')

    let request = require('request')
    let options = {
        url: `http://api.postcode.nl/rest/addresses/${this.address.postcode}/${this.address.houseNumber}/${this.address.houseNumberAddition || ''}`,
        headers: {
            'User-Agent': 'request',
            'Authorization': auth
        }
    }

    let _this = this

    request(options, function callback(error, response, body) {
        if (error || response.statusCode !== 200) {
            console.debug('[User Model] failed to get address data from postcode.nl')
            console.debug('[User Model] %s', error)
            console.debug('[User Model] %s', body)
            return next()
        }

        console.debug('[User Model] got response code: %s', response.statusCode)
        console.debug('[User Model] address: %s', body)
        if (response.statusCode === 200) {
            body = JSON.parse(body)
            let _ = require('underscore')
            _this.address = _.assign(_this.address, body)
            _this.markModified('address')
        }
        next()
    })
})
/**
 * Methods
 */
UserSchema.methods = {
    /**
     * Authenticate - check if the passwords are the same
     *
     * @param {String} password
     * @param {Function} callback
     * @return {Boolean}
     * @api public
     */
    authenticate: function (password, callback) {
        console.log("*&*&*&* authenticate --> ", password)

        if (!callback) {
            return this.password === this.encryptPassword(password)
        }

        let _this = this
        this.encryptPassword(password, function (err, pwdGen) {
            if (err) {
                callback(err)
            }

            if (_this.password === pwdGen) {
                callback(null, true)
            } else {
                callback(null, false)
            }
        })
    },

    /**
     * Make salt
     *
     * @param {Number} byteSize Optional salt byte size, default to 16
     * @param {Function} callback
     * @return {String}
     * @api public
     */
    makeSalt: function (byteSize, callback) {
        let defaultByteSize = 16

        if (typeof arguments[0] === 'function') {
            callback = arguments[0]
            byteSize = defaultByteSize
        } else if (typeof arguments[1] === 'function') {
            callback = arguments[1]
        }

        if (!byteSize) {
            byteSize = defaultByteSize
        }

        if (!callback) {
            return crypto.randomBytes(byteSize).toString('base64')
        }

        return crypto.randomBytes(byteSize, function (err, salt) {
            if (err) {
                callback(err)
            }
            return callback(null, salt.toString('base64'))
        })
    },

    /**
     * Encrypt password
     *
     * @param {String} password
     * @param {Function} callback
     * @return {String}
     * @api public
     */
    encryptPassword: function (password, callback) {
        if (!password || !this.salt) {
            return null
        }

        let defaultIterations = 10000
        let defaultKeyLength = 64
        let salt = new Buffer(this.salt, 'base64')

        if (!callback) {
            return crypto.pbkdf2Sync(password, salt, defaultIterations, defaultKeyLength)
                .toString('base64')
        }

        return crypto.pbkdf2(password, salt, defaultIterations, defaultKeyLength, function (err, key) {
            if (err) {
                callback(err)
            }
            return callback(null, key.toString('base64'))
        })
    }
}

module.exports = mongoose.model('User', UserSchema)
