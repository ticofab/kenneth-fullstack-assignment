'use strict'
let should = require('should')
let Model = require('../../')

let User = Model.User
let UserMock = User._mocks

/* global describe, it, before */
describe('User Model: ', function () {
  describe('creation ', function () {
    [{}, {isOperator: true}].forEach(function (userData) {
      xit('should fail if required fields are not set', function (done) {
        User
          .create(userData)
          .then(function (user) {
            should.not.exist(user)
            done()
          })
          .catch(function (err) {
            should.exist(err)
            err.name.should.eql('ValidationError')
            done()
          })
      })
    })

    xit('should succeed saving a minimal mock object', function (done) {
      let userData = UserMock.getMinimal()
      User.create(userData).then(function (user) {
        should.exist(user)
        done()
      }, done)
    })

    xit('should succeed saving a full blown mock object', function (done) {
      let userData = UserMock.get()
      User.create(userData).then(function (user) {
        should.exist(user)
        done()
      }, done)
    })

    xit('should fail if a User with the same phoneNumber exists', function (done) {
      let data = UserMock.getMinimal({phoneNumber: '31123456789'})
      delete data._id

      User.create(data).then(function () {
        return User.create(data)
      })
      .then(function (result) {
        should.not.exist(result)
        done()
      }, function (err) {
        should.exist(err)
        err.name.should.eql('ValidationError')
        done()
      })
      .catch(done)
    })

    let badInput = ['hodor', '1', '123', null, undefined]
    badInput.forEach(function (data) {
      xit('should fail if phoneNumber is invalid', function (done) {
        data = UserMock.getMinimal({phoneNumber: data})
        User.create(data).then(function (user) {
          should.not.exist(user)
          done()
        })
        .catch(function (err) {
          should.exist(err)
          console.error(err)
          err.name.should.eql('ValidationError')
          done()
        })
        .catch(done)
      })
    })

    let badNames = [{firstName: 'h1lde'}, {lastName: 'h0d0r'}]
    badNames.forEach(function (userData) {
      xit('should fail if name contains numbers', function (done) {
        userData = UserMock.getMinimal(userData)
        User.create(userData).then(function (user) {
          should.not.exist(user)
          done()
        })
        .catch(function (err) {
          should.exist(err)
          console.error(err)
          err.name.should.eql('ValidationError')
          done()
        })
        .catch(done)
      })
    })
  })

  describe('.authenticate', function () {
    const PASSWORD = 'hodor'
    let user
    before(function (done) {
      User.create(UserMock.getMinimal({password: PASSWORD}))
      .then(function (result) {
        user = result
        done()
      }, done)
    })

    xit('should return true if passwords do not match', function (done) {
      user.authenticate(PASSWORD).should.eql(true)
      user.authenticate(PASSWORD, function (err, result) {
        should.not.exist(err)
        result.should.eql(true)
        done()
      })
    })

    xit('should return false if passwords do not match', function (done) {
      user.authenticate('-----').should.eql(false)
      user.authenticate('-----', function (err, result) {
        should.not.exist(err)
        result.should.eql(false)
        done()
      })
    })

    xit('should return error password is not a string', function (done) {
      try {
        user.authenticate(123)
      } catch (err) {
        should.exist(err)
        return done()
      }
      done(new Error('should throw error'))
    })
  })
})
