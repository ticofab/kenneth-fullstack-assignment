'use strict'
require('logger')
let path = require('path')
let fs = require('fs')

// console.debug = winston.debug

function printPaths(model) {
    let paths = ''
    model.schema.eachPath(function (path) {
        paths += '\n\t' + path
    })
    return paths
}

class Model {
    constructor() {
        console.info('[Model] Initializing')
        let normalizedPath = path.join(__dirname, 'models')

        let _this = this
        fs.readdirSync(normalizedPath).forEach(function (file) {
            let modelName = file.charAt(0).toUpperCase() + file.slice(1)
            let modelPath = path.join(normalizedPath, file)
            console.info('[Model] Setting up ' + modelName,modelPath)
            _this[modelName] = require(modelPath)
            // console.info(printPaths(_this[modelName]))
        })

        console.info('[Model] Done')
        this.User = require('./models/user')
        this.Forecast = require('./models/forecast')
    }

    register(modelPath) {

    }
}

let model = new Model()
module.exports = exports = model
