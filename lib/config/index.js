'use strict'
require('logger')

console.info('[Config] Initializing')
let path = require('path')
let _ = require('lodash')

let root = path.normalize(process.cwd())
let env = process.env.NODE_ENV

console.info('[Config] running in %s mode from %s', env, root)

// All configurations will extend these options
// ============================================
let all = {
  env: process.env.NODE_ENV,

  // Root path of server
  root: path.normalize(__dirname + '/../../..'),

  // Server port
  port: process.env.PORT || 3000,

  // Should we populate the DB with sample data?
  seedDB: process.env.SEED || false,

  // Logging
  log: process.env.LOG_LEVEL || 'debug',
  logDirectory: process.env.LOGDIR || '.',
  // Secret for session, you will want to change this and make it an environment variable
  secrets: {
    session: 'api-secret'
  },

  // List of user roles
  userRoles: ['guest', 'user', 'client', 'worker', 'operator', 'admin'],

  // MongoDB connection options
  mongo: {
    uri: process.env.MONGOLAB_URI ||
    'mongodb://localhost',
    options: {
      db: {
        safe: true
      }
    }
  },

  // Oauth providers
  facebook: {
    clientID: process.env.FACEBOOK_ID || 'id',
    clientSecret: process.env.FACEBOOK_SECRET || 'secret',
    callbackURL: (process.env.DOMAIN || '') + '/auth/facebook/callback'
  },

  twitter: {
    clientID: process.env.TWITTER_ID || 'id',
    clientSecret: process.env.TWITTER_SECRET || 'secret',
    callbackURL: (process.env.DOMAIN || '') + '/auth/twitter/callback'
  },

  google: {
    clientID: process.env.GOOGLE_ID || 'id',
    clientSecret: process.env.GOOGLE_SECRET || 'secret',
    callbackURL: (process.env.DOMAIN || '') + '/auth/google/callback'
  }
}

// Export the config object based on the NODE_ENV
// ==============================================
module.exports = _.merge(
  all,
  require('./environments/' + process.env.NODE_ENV + '.js') || {})

console.info('[Config] Done')
